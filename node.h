#ifndef NODE_H
#define NODE_H

#include <stdio.h>
#include <stdlib.h>

struct Node {
	void *data;
	struct Node *next;
	struct Node *prev;
};

void initializeNode(struct Node *);

void setNext(struct Node *nodePtr, struct Node *next);
void setPrev(struct Node *nodePtr, struct Node *prev);
void setData(struct Node *nodePtr, void *data);

struct Node *getNext(struct Node *);
struct Node *getPrev(struct Node *);
void *getData(struct Node *);

#endif
