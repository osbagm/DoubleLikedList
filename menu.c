#include <stdio.h>
#include <stdlib.h>

#include "menu.h"

struct List myList;

void initialize() {
	initializeList(&myList);
}

int selectDataType() {
	const int OPTIONS_NUMBER = 4;
	int option = 0;
	int validOption = 0;

	while(!validOption) {
		clearScreen();
		printf("DOUBLE LINED LIST\n\n");

		printf("1.Cadena de caracteres\n");
		printf("2.Enteros\n");
		printf("3.Decimales\n");
		printf("4.Salir\n\n");
		printf("Selecciona un numero de acuerdo a la opcion: (1-5): ");
		scanf("%d", &option);
		getchar();

		if(option > OPTIONS_NUMBER || option < 1) {
			validOption = 0;
		} else {
			validOption = 1;
		}
	}
	return option;
}

void clearScreen() {
	for(int counter = 0; counter < 30; counter ++) {
		printf("\n");
	}
}

int showMenu() {
	const int OPTIONS_NUMBER = 5;
	int option = 0;
	int validOption = 0;


	while(!validOption) {
		clearScreen();
		printf("DOUBLE LINKED LIST\n\n");
		printf("1.Agregar elemento\n");
		printf("2.Eliminar elemento\n");
		printf("3.Ver lista\n");
		printf("4.Eliminar Lista\n");
		printf("5.Salir\n\n");

		printf("Selecciona un numero de acuerdo a la opcion: (1-5): ");
		scanf("%d", &option);
		getchar();
		if(option > OPTIONS_NUMBER || option < 1) {
			validOption = 0;
			clearScreen();
		} else {
			validOption = 1;
		}
	}

	return option;
}

void addElementOption() {
	void *element = NULL;
	int *aPtr = (int *) malloc(sizeof(int));

	clearScreen();
	printf("\nIngresa el elemento a guardar: ");
	scanf("%d", aPtr);
	element = aPtr;
	getchar();

	insertElement(&myList, getLastPos(&myList), element);
	printf("\nElemento insertado!\n");
	printList(&myList);
	getchar();
}

void printListOption() {
	printList(&myList);
	getchar();
}
