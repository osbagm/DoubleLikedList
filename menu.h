#ifndef MENU_H
#define MENU_H

#include "list.h"


void initialize();
int selectDataType();
int showMenu();

void clearScreen();

void addElementOption();
void deleteElementOption();
void printListOption();
void deleteListOption();

#endif
