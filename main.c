#include <stdio.h>
#include <stdlib.h>

#include "list.h"
#include "menu.h"

#define ADD 1
#define DELETE_ELEMENT 2
#define PRINT_LIST 3
#define DELETE_LIST 4
#define EXIT 5


int main() {
	int option = 0;

	initialize();

	while(option != EXIT) {
		option = showMenu();

		switch(option) {
			case ADD:
				addElementOption();
				break;
			case DELETE_ELEMENT:
				break;
			case PRINT_LIST:
				printListOption();
				break;
			case DELETE_LIST:
				break;
			case EXIT:
				break;
			default:
				break;
		}
	}

	return 0;
}
