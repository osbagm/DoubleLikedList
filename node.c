#include "node.h"

void initializeNode(struct Node *ptr) {
	ptr->data = NULL;
	ptr->next = NULL;
	ptr->prev = NULL;

}

void setNext(struct Node *nodePtr, struct Node *next){
	nodePtr->next = next;
}

void setPrev(struct Node *nodePtr, struct Node *prev){
	nodePtr->prev = prev;
}

void setData(struct Node *nodePtr, void  *data){
	nodePtr->data = data;
}

struct Node *getNext(struct Node *nodePtr) {
	return nodePtr->next;
}

struct Node *getPrev(struct Node *nodePtr) {
	return nodePtr->prev;
}

void *getData(struct Node *nodePtr) {
	return nodePtr->data;
}


