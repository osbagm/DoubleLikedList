#ifndef LIST_H
#define LIST_H

#include "node.h"

struct List {
	struct Node *header;
};

void initializeList(struct List *);
void insertElement(struct List *, struct Node *, void *);
void deleteElement(struct List *, struct Node *);
void *retrieveElement(struct Node *);
void printList(struct List *);

void deleteList(struct List *);

int isValidPos(struct List *, struct Node *);

struct Node *getLastPos(struct List *);
struct Node *getFirstPos(struct List *);

#endif

