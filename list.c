#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

#include "list.h"

void initializeList(struct List *listPtr) {
	listPtr->header = (struct Node *) malloc(sizeof(struct Node));
	if(listPtr->header == NULL) {
		printf("\nERROR al inicializar la lista\n");
		return;
	}
	initializeNode(listPtr->header);

	listPtr->header->next = listPtr->header;
	listPtr->header->prev = listPtr->header;
}

int isValidPos(struct List *listPtr, struct Node *pos) {
	struct Node *aux = getNext(listPtr->header);

	if(pos == listPtr->header) {
		return 1;
	}

	while(aux != listPtr->header) {
		if(aux == pos) {
			return 1;
		}
		aux = getNext(aux);
	}
	return 0;
}

void insertElement(struct List *listPtr, struct Node *pos, void *data) {

	if(!isValidPos(listPtr, pos)) {
		printf("\nPosicion NO valida para insertar elemento\n");
		return;
	}

	struct Node *aux = NULL;
	aux = (struct Node *)malloc(sizeof(struct Node));
	if(aux == NULL) {
		printf("\nNo se pudo crear otro Nodo al insertar elemento\n");
		return;
	}

	setData(aux, data);

	setNext(aux, getNext(pos));
	setPrev(aux, pos);
	setPrev(getNext(pos), aux);
	setNext(pos, aux);

}

void deleteElement(struct List *listPtr, struct Node *pos) {
	if(!isValidPos(listPtr, pos)) {

		printf("\nPosicion invalida al eliminar\n");

		return;
	}

	setNext(getPrev(pos), getNext(pos));
	setPrev(getNext(pos), getPrev(pos));

	free(pos);

}

void *retrieveElement(struct Node *nodePtr) {
	return getData(nodePtr);
}

void printList(struct List *listPtr) {
	int counter = 0;
	struct Node *aux = NULL;
	aux = getNext(listPtr->header);

	while(aux != listPtr->header) {
		printf("\n%d", *(int *)getData(aux));
		aux = getNext(aux);
		counter++;
	}
	printf("\n");
	printf("Se contaron %d elemento(s)\n", counter);
}

struct Node *getLastPos(struct List *listPtr) {
	return getPrev(listPtr->header);
}

struct Node *getFirstPos(struct List *listPtr) {
	return getNext(listPtr->header);
}

void deleteList(struct List *listPtr) {
	struct Node *aux = getFirstPos(listPtr);

	while(aux != listPtr->header) {
		setNext(getPrev(aux), getNext(aux));
		setPrev(getNext(aux), getPrev(aux));

		free(aux);

		aux = getNext(aux);
	}

	setNext(listPtr->header, listPtr->header);
	setPrev(listPtr->header, listPtr->header);

}
